# Room_Escape
 
This is an escape room game written Java that takes place in an undersea facility.

## Installation
To use:
1. download the zip file
1. extract the files
1. If not already installed, use the installation file (jre-8u202-windows-x64.exe) to install the Java Runtime Environment needed to run this application.
1. run the application (escape.exe) file

## How to Use
```
p --- pause/play music
m --- display mouse coordinates in the upper right corner of the screen
esc --- quit/exit without saving
primarily mouse based controls
some objects in the inventory can be activated by right clicking the icon in the inventory
```

Explore the rooms and interact with any object that shows a label when the mouse is hovered over.

## Credits
* game engine provided by Matthew Phillips, California State University of Sacramento
* story by Joshua Juarros
* original artwork
* original music created using [Music Maker Jam](https://www.musicmakerjam.app/)